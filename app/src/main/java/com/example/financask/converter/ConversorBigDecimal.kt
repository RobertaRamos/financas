package com.example.financask.converter

import androidx.room.TypeConverter
import java.math.BigDecimal

class ConversorBigDecimal {
    @TypeConverter
    fun paraLong(valor: BigDecimal?): Long? {
        return valor?.toLong()
    }

    @TypeConverter
    fun paraBigDecimal(valor: Long?): BigDecimal? {
        return valor?.let { BigDecimal.valueOf(it.toDouble()) }
    }
}