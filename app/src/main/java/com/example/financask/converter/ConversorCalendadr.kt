package com.example.financask.converter

import androidx.room.TypeConverter
import androidx.room.TypeConverters
import java.util.*


class ConversorCalendadr {

    @TypeConverter
    fun paraLong(valor: Calendar?): Long? {
        return valor?.timeInMillis
    }

    @TypeConverter
    fun paraCalendar(valor: Long?): Calendar? {
        val momentoAtual = Calendar.getInstance()
        if (valor != null) {
            momentoAtual.timeInMillis = valor
        }
        return momentoAtual
    }
}