package com.example.financask.asynctask

import android.os.AsyncTask
import com.example.financask.DAO.TransacaoDataBaseDao
import com.example.financask.model.Transacao

@Suppress("DEPRECATION")
class SalvaAlunoTask(
    private val dao: TransacaoDataBaseDao,
    private val transacao: Transacao,
    private val listiner: FinalizadoListiner) : AsyncTask<Void, Void, Void>() {


    override fun doInBackground(vararg params: Void?): Void? {
        dao.salva(transacao)
        return null
    }

    override fun onPostExecute(result: Void?) {
        super.onPostExecute(result)
        listiner.quandoFinalizado()
    }

    interface FinalizadoListiner {
        fun quandoFinalizado()
    }
}