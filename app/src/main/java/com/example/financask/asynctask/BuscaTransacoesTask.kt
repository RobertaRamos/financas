package com.example.financask.asynctask

import android.os.AsyncTask
import com.example.financask.DAO.TransacaoDataBaseDao
import com.example.financask.model.Transacao

class BuscaTransacoesTask(private val dao: TransacaoDataBaseDao,
                          private val listiner: FinalizadoListiner) : AsyncTask<Void, Void, MutableList<Transacao>>() {

    override fun doInBackground(vararg params: Void?): MutableList<Transacao> {
       return dao.todas()
    }

    override fun onPostExecute(transcoes: MutableList<Transacao>?) {
        super.onPostExecute(transcoes)
        if (transcoes != null) {
            listiner.quandoFinalizado(transcoes)
        }

    }

    interface FinalizadoListiner {
        fun quandoFinalizado(transacoes: MutableList<Transacao>)
    }
}