package com.example.financask.ui.activity

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import androidx.appcompat.app.AppCompatActivity
import com.example.financask.DAO.TransacaoDataBaseDao
import com.example.financask.R
import com.example.financask.asynctask.BuscaTransacoesTask
import com.example.financask.asynctask.EditaAlunoTask
import com.example.financask.asynctask.RemoveTransacaoTask
import com.example.financask.asynctask.SalvaAlunoTask
import com.example.financask.database.TransacaoDataBase
import com.example.financask.model.Tipo
import com.example.financask.model.Transacao
import com.example.financask.ui.ResumoView
import com.example.financask.ui.adapter.ListaTransacoesAdapter
import com.example.financask.ui.dialog.AdicionaTransacaoDialog
import com.example.financask.ui.dialog.AlteraTransacaoDialog
import kotlinx.android.synthetic.main.activity_lista_transacoes.*
import kotlinx.android.synthetic.main.form_transacao.view.*
import java.util.*

class ListaTransacoesActivity : AppCompatActivity() {

    private lateinit var dao: TransacaoDataBaseDao
    private var listaTransacoes: MutableList<Transacao> = mutableListOf()
    private lateinit var listaTransacaoAdapter: ListaTransacoesAdapter

    private val viewDaActivity by lazy {
        window.decorView
    }
    private val vieuGroupDaActivity by lazy {
        viewDaActivity as ViewGroup
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_lista_transacoes)
        dao = TransacaoDataBase.getInstancia(this).getTransacaoDataBaseDao()
        buscaTodasTransacoes()

    }

    fun buscaTodasTransacoes(){
        BuscaTransacoesTask(dao, object : BuscaTransacoesTask.FinalizadoListiner{
            override fun quandoFinalizado(transacoes: MutableList<Transacao>) {
                listaTransacoes = transacoes
                configuraLista()
                configuraResumo()
                configuraFab()
            }

        }).execute()

    }


    private fun configuraFab() {
        lista_transacoes_adiciona_receita.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {
                chamaDialogDeAdicao(Tipo.RECEITA)
            }
        })

        lista_transacoes_adiciona_despesa.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {
                chamaDialogDeAdicao(Tipo.DESPESA)
            }
        })
    }


    private fun chamaDialogDeAdicao(tipo: Tipo) {
        AdicionaTransacaoDialog(vieuGroupDaActivity, this@ListaTransacoesActivity)
            .chama(tipo) { transacaoCriada ->
                adiciona(transacaoCriada)
                lista_transacoes_adiciona_menu.close(true)
            }

    }

    private fun adiciona(transacao: Transacao) {
        SalvaAlunoTask(dao, transacao, object : SalvaAlunoTask.FinalizadoListiner{
            override fun quandoFinalizado() {
                atualizaTransacoes()
            }

        }).execute()

    }

    private fun atualizaTransacoes() {
        buscaTodasTransacoes()
        configuraLista()
        configuraResumo()
    }


    private fun configuraResumo() {
        val resumoView = ResumoView(vieuGroupDaActivity, listaTransacoes, this)
        resumoView.atualiza()
    }

    private fun configuraLista() {
        listaTransacaoAdapter = (ListaTransacoesAdapter(listaTransacoes, this))

        with(lista_transacoes_listview) {
            adapter = listaTransacaoAdapter
            lista_transacoes_listview.setOnItemClickListener(object :
                AdapterView.OnItemClickListener {
                override fun onItemClick(
                    parent: AdapterView<*>?,
                    view: View?,
                    position: Int,
                    id: Long
                ) {
                    val transacaoClicada = listaTransacoes[position]
                    chamaDialogDeAlteracao(transacaoClicada)
                }
            })

            setOnCreateContextMenuListener { menu, View, menuInfo ->
                menu.add(Menu.NONE, 1, Menu.NONE, "Remover")
            }
        }

    }

    override fun onContextItemSelected(item: MenuItem): Boolean {
        val idDoMenu = item.itemId

        if(idDoMenu == 1){
            val adaptertMenuInfo = item.menuInfo as AdapterView.AdapterContextMenuInfo
            val posicaoDaTransacao = adaptertMenuInfo.position
            val transacao = listaTransacoes[posicaoDaTransacao]
            removeNoAdapter(posicaoDaTransacao)
            remove(transacao)

        }
        return super.onContextItemSelected(item)
    }

    private fun removeNoAdapter(posicaoDaTransacao: Int) {
        listaTransacaoAdapter.remover(posicaoDaTransacao)
    }

    private fun remove(transacao: Transacao) {
        RemoveTransacaoTask(dao, transacao, object : RemoveTransacaoTask.FinalizadoListiner{
            override fun quandoFinalizado() {
                atualizaTransacoes()
            }
        }).execute()


    }

    private fun chamaDialogDeAlteracao(transacaoClicada: Transacao) {
        AlteraTransacaoDialog(vieuGroupDaActivity, this@ListaTransacoesActivity)
            .chama(transacaoClicada) { transacaoAlterda ->
                adicionaId(transacaoClicada, transacaoAlterda)
                altera(transacaoAlterda)
            }
    }

    private fun adicionaId(transacaoClicada: Transacao, transacaoAlterda: Transacao) {
        val id = transacaoClicada.id
        transacaoAlterda.id = id
    }

    private fun altera(transacao: Transacao) {
        EditaAlunoTask(dao, transacao, object : EditaAlunoTask.FinalizadoListiner{
            override fun quandoFinalizado() {
                atualizaTransacoes()
            }

        }).execute()


    }

}


