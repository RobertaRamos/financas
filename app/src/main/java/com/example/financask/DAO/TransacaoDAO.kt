package com.example.financask.DAO

import com.example.financask.model.Transacao

class TransacaoDAO {

    val listaTransacoes: List<Transacao> = Companion.listaTransacoes

    companion object{
        private val listaTransacoes: MutableList<Transacao> = mutableListOf()
    }

    fun adiciona (transacao: Transacao){
        Companion.listaTransacoes.add(transacao)
    }

    fun altera (transacao: Transacao, posicao: Int){
        Companion.listaTransacoes[posicao] = transacao
    }

    fun remove (posicao: Int){
        Companion.listaTransacoes.removeAt(posicao)
    }
}