package com.example.financask.DAO

import androidx.room.*
import com.example.financask.model.Transacao

@Dao
interface TransacaoDataBaseDao {

    @Insert
    fun salva(transacao: Transacao)

    @Query("SELECT * FROM Transacao")
    fun todas(): MutableList<Transacao>

    @Delete
    fun delete(transacao: Transacao)

    @Update
    fun edita(transacao: Transacao)

}