package com.example.financask.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.example.financask.DAO.TransacaoDataBaseDao
import com.example.financask.converter.ConversorBigDecimal
import com.example.financask.converter.ConversorCalendadr
import com.example.financask.model.Transacao

@Database(entities = [Transacao::class], version = 1, exportSchema = false)
@TypeConverters(ConversorCalendadr::class, ConversorBigDecimal::class)
abstract class TransacaoDataBase : RoomDatabase() {

    abstract fun getTransacaoDataBaseDao(): TransacaoDataBaseDao

    companion object {
        fun getInstancia(context: Context): TransacaoDataBase {
            return Room.databaseBuilder(context, TransacaoDataBase::class.java, "Transacao.db")
                .build()
        }
    }

}