package com.example.financask.extension

import java.math.BigDecimal
import java.text.DecimalFormat
import java.util.Locale

fun BigDecimal.formataMoedaParaBrasileiro(): String {
    val formatoMoedaBrasileiro = DecimalFormat.getCurrencyInstance(Locale("pt", "br"))
    return formatoMoedaBrasileiro.format(this).replace("R$", "R$ ").replace("-R$ ","R$ -")

}