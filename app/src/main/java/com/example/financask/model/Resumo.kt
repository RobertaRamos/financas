package com.example.financask.model

import java.math.BigDecimal

class Resumo(private val listaTransacoes: List<Transacao>) {

    val receita get() = somaPor(Tipo.RECEITA)

    val despesa get() = somaPor(Tipo.DESPESA)

    val total get() = receita.subtract(despesa)

    private fun somaPor(tipo: Tipo): BigDecimal{
        val somaDeTransacoesPeloTipo  = listaTransacoes.filter { it.tipo == tipo }
            .sumOf { it.valor.toDouble() }
        return BigDecimal(somaDeTransacoesPeloTipo)
    }
}