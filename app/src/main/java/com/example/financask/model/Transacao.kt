package com.example.financask.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.math.BigDecimal
import java.util.Calendar

@Entity
class Transacao(
    val valor: BigDecimal,
    val categoria: String = "Indefinida",
    val tipo: Tipo,
    val data: Calendar = Calendar.getInstance()){

    @PrimaryKey(autoGenerate = true)
    var id: Int = 0

}





